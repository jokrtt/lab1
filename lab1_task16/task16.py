import os
from os import environ as env
# часть 1 задание 16

class Conventer:
    def __init__(self, rub=91.94):
        self.rub: float = rub

    def convert(self, value: float, toType: str = "usd") -> str:
        if toType.lower() == "usd":
            return f"Convert {value} rub to usd -> {round(value / self.rub, 2)} usd"
        elif toType.lower() == "rub":
            return f"Convert {value} usd to rub -> {round(value * self.rub, 2)} rub"

if __name__ == "__main__":
    rub = float(env.get("rub"))
    value = float(env.get("value"))
    type = str(env.get("type"))

    conv = Conventer(rub)
    with open ("result_task1.txt","w") as file:
        file.write(conv.convert(value,type))
