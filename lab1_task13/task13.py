import psycopg2
import time
import xlwt
import datetime
from os import environ as env
try:
    time.sleep(10)

    conn = psycopg2.connect(
        dbname="root",
        user="root",
        password="root",
        host="172.19.0.2",
        port="5432"
    )

    cur = conn.cursor()
    exam = str(env.get("exam"))
    query = f"""
        SELECT DISTINCT TO_CHAR(CAST(exam_date AS DATE), 'YYYY-MM-DD') as formatted_date FROM gradebook WHERE subject='{exam}';
    """

    cur.execute(query)
    rows = cur.fetchall()
    workbook = xlwt.Workbook()
    sheet = workbook.add_sheet('Exam Dates')

    with open("result.txt","w") as file:
        file.write(exam)
        file.write(str(rows))
    
    sheet.write(0, 0, exam)


    for row_idx, row in enumerate(rows, start=1):
        sheet.write(row_idx, 0, row)

    workbook.save('exam_dates.xls')

    conn.commit()

except Exception as e:
    print(f"Error: {e}")

finally:
    cur.close()
    conn.close()